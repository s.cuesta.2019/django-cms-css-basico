from django.shortcuts import render
from django.http import HttpResponse
import random

def index_page(request):
    return HttpResponse("Esta es la página principal de mi sitio web. Pregunta por /mainpage/ o /main.css")

def main_page(request):
    content = "Esta es la página 'MAIN PAGE' de mi sitio web."
    return render(request, "cms/main.html", {"content": content})

def estilo_css(request):
    colors = ["blue", "yellow", "red", "green", "black"]
    color = random.choice(colors)
    background = random.choice(colors)

    css_content = f"""
    body {{
        margin: 10px 20% 50px 70px;
        font-family: sans-serif;
        color: {color};
        background: {background};
    }}
    """
    response = HttpResponse(css_content, content_type="text/css")
    return response
